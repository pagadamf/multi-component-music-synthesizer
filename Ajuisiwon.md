#  Ajuisiwon Azantilow-Effects
An effect is an audio processor.  It accepts audio input and produces an audio output 
that is a modification of the input.  Common effects include echos, reverb, and others.  
For this project, the effects implemented are the flange, noise gating, reverb and chorus effects for the audio.
