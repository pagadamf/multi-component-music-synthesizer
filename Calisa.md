#  Calisa Xu Stevens-Piano Synthesizer
There are a variety of ways to simulate a piano. 
Most of them are based on the idea of recorded samples of the piano playing notes.  
However, creating a realistic sounding piano is more complicated than than.  For this component 
we simulated the sound of an acoustic piano


## Piano Score Formatting
```
<?xml version="1.0" encoding="utf-8"?>
<score bpm="60" beatspermeasure="4">9
	<instrument instrument="PianoInstrument">
		<note measure="1" beat="1" duration="1" dynamic="1" note="Step5/piano/W42.wav"/>
		<note measure="1" beat="2" duration="1" note="Step5/piano/W42.wav"/>
		<note measure="1" beat="3" duration="1" dynamic="2" note="Step5/piano/W36.wav"/>
		<note measure="1" beat="4" duration="1" pedal="pressed" note="Step5/piano/W32.wav"/>
		<note measure="2" beat="1" duration="1" note="Step5/piano/E60.wav" advdynamic="Step5/piano/E6l.wav"/>
		<note measure="2" beat="2" duration="1" pedal="released" note="Step5/piano/W33.wav"/>
		<note measure="2" beat="3" duration="1" note="Step5/piano/E60.wav" advdynamic="Step5/piano/E6l.wav"/>
		<note measure="2" beat="4" duration="1" dynamic="3" note="Step5/piano/W55.wav"/>
	</instrument>
</score>```