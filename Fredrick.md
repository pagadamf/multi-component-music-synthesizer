#  Fredrick Pagadam-Additive Synthesizer
Additive synthesis works by adding together sinusoids to make a sound.  
As discussed in class, any sound can be made by adding together sinusoids.  
An additive synthesizer attacks sound generation by directly using that idea

All the attributes for the additive were implemented to work except for the 
crossfade which couldn't get to work as expected

## Adder Score Formatting
```
<?xml version="1.0" encoding="utf-8"?>
<score bpm="120" beatspermeasure="2">
	<instrument instrument="AdditiveInstrument">
       <note measure="1" beat="2" duration="0.33" harmonics="1 0 0 0 0" note="F4"/>
       <note measure="1" beat="2.33" duration="0.33" harmonics="1 0 0 0 0" note="G4"/>
       <note measure="1" beat="2.6" duration="0.33" harmonics="1 0 0 0 0" note="A4"/>
       <note measure="2" beat="1" duration="0.5" harmonics="1 0 0 0 0" note="Bb4"/>
       <note measure="2" beat="1" duration="0.5" harmonics="1 0 0 0 0" note="G4"/>
       <note measure="2" beat="1" duration="0.5" harmonics="1 0 0 0 0" envelope=".05 0.85 .99 .01"  note="Eb4"/>
       <note measure="3" beat="1" duration="0.5" harmonics="1 0 0 0 0" envelope=".05 0.85 .99 .01"  note="Bb4"/>
       <note measure="3" beat="1" duration="0.5" harmonics="1 0 0 0 0" envelope=".05 0.85 .99 .01" note="G4"/>
       <note measure="3" beat="1" duration="0.5" harmonics="1 0 0 0 0" envelope=".05 0.85 .99 .01" note="Eb4"/>
       <note measure="4" beat="1" duration="0.5" harmonics="1 0 0 0 0" envelope=".05 0.85 .99 .01" note="Bb4"/>
       <note measure="4" beat="1" duration="0.5" harmonics="1 0 0 0 0" envelope=".05 0.85 .99 .01" note="D4"/>
       <note measure="4" beat="1" duration="0.5" harmonics="1 0 0 0 0" envelope=".05 0.85 .99 .01" note="F4"/>
       <note measure="4" beat="2" duration="0.5" harmonics="1 0 0 0 0" envelope=".05 0.85 .99 .01" note="Bb4"/>
       <note measure="4" beat="2" duration="0.5" harmonics="1 0 0 0 0" envelope=".05 0.85 .99 .01" note="G4"/>
       <note measure="4" beat="2" duration="0.5" harmonics="1 0 0 0 0" envelope=".05 0.85 .99 .01" note="Eb4"/>
       <note measure="5" beat="1" duration="0.5" harmonics="1 0 0 0 0" envelope=".05 0.85 .99 .01" note="Bb4"/>
       <note measure="5" beat="1" duration="0.5" harmonics="1 0 0 0 0" envelope=".05 0.85 .99 .01" note="D4"/>
       <note measure="5" beat="1" duration="0.5" harmonics="1 0 0 0 0" envelope=".05 0.85 .99 .01" note="F4"/>
       <note measure="6" beat="1" duration="0.5" harmonics="1 0 0 0 0" envelope=".05 0.85 .99 .01" note="G4"/>
       <note measure="6" beat="1.5" duration="0.5" harmonics="1 0 0 0 0" envelope=".05 0.85 .99 .01" note="F#4"/>
       <note measure="6" beat="2" duration="0.5" harmonics="1 0 0 0 0" envelope=".05 0.85 .99 .01" note="G4"/>
       <note measure="6" beat="2.5" duration="0.5" harmonics="1 0 0 0 0" vibrato="5 10" note="G#4"/>
       <note measure="7" beat="1" duration="1" harmonics="1 0 0 0 0" vibrato="5 10" note="A4"/>
       <note measure="7" beat="2" duration="1" harmonics="1 0 0 0 0" vibrato="5 10" note="F4"/>
       <note measure="8" beat="1" duration="3" harmonics="1 0 0 0 0" vibrato="5 10" note="Bb4"/>
       <note measure="8" beat="1" duration="3" harmonics="1 0 0 0 0" vibrato="5 10" note="F4"/>
       <note measure="8" beat="1" duration="3" harmonics="1 0 0 0 0" vibrato="5 10" note="D4"/>
    </instrument>
</score>
```
