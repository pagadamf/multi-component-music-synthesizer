#  Becca Winkler-Organ Synthesizer
Organ synthesis is very closely related to additive synthesis, using combinations of sinusoids to make a sound, 
but with some specializations, particularly in the choice of harmonics and some sound modifications.  For this 
component you are required to simulate the sound of a Hammond B3 Organ.

# Organ Score Format
```
## Organ Score Formatting
<?xml version="1.0" encoding="utf-8"?>
<score bpm="60" beatspermeasure="4">
  <instrument instrument="Organ">
	  <note measure="1" beat="1" duration="0.8" note="A3" drawbar="805020000"/>
	  <note measure="1" beat="2" duration="0.75" note="A3" drawbar="805020000"/>
	  <note measure="1" beat="3" duration="0.75" note="F3" drawbar="805020000"/>
	  <note measure="1" beat="4" duration="0.8" note="E3" drawbar="805020000"/>
	  <note measure="2" beat="1" duration="0.75" note="C3" drawbar="805020000"/>
	  <note measure="2" beat="2" duration="1" note="E3" drawbar="805020000"/>
	  <note measure="2" beat="3" duration="0.8" note="C3" drawbar="805020000"/>
	  <note measure="2" beat="4" duration="0.75" note="A3" drawbar="805020000"/>
	  <note measure="3" beat="1" duration="0.75" note="A3" drawbar="805020000"/>
	  <note measure="3" beat="2" duration="0.75" note="F3" drawbar="805020000"/>
	  <note measure="3" beat="3" duration="0.75" note="E3" drawbar="805020000"/>
	  <note measure="3" beat="4" duration="0.8" note="C3" drawbar="805020000"/>
	  <note measure="4" beat="1" duration="1" note="E3" drawbar="805020000"/>

  </instrument>
</score>
```