# A Multi-Component Music Synthesizer

This is the page for the group project for CSE471 Fall 2023
-  [Becca Winkler][1] Organ Synthesizer
-  [Ajuisiwon Azantilow][2] Effects
-  [Calisa Xu Stevens][3] Piano Synthesizer
-  [Fredrick Pagadam][4] Additive Synthesizer
## Selection Score
```
<?xml version="1.0" encoding="utf-8"?>
<score bpm="60" beatspermeasure="4">
  <instrument instrument="Organ">
  <note measure="1" beat="1" duration="0.75" note="G3" drawbar="805020000"/>
	  <note measure="1" beat="2" duration="0.75" note="C3" drawbar="805020000"/>
	  <note measure="1" beat="3" duration="0.75" note="E3" drawbar="805020000"/>
	  <note measure="1" beat="4" duration="1" note="A3" drawbar="805020000"/>
	  <note measure="2" beat="1" duration="0.75" note="G3" drawbar="805020000"/>
	  <note measure="2" beat="2" duration="0.75" note="C3" drawbar="805020000"/>
	  <note measure="2" beat="3" duration="0.75" note="E3" drawbar="805020000"/>
	  <note measure="2" beat="4" duration="1" note="A3" drawbar="805020000"/>
      <note measure="5" beat="1" duration="0.75" note="G3" drawbar="805020000"/>
	  <note measure="5" beat="2" duration="0.75" note="C3" drawbar="805020000"/>
	  <note measure="5" beat="3" duration="0.75" note="E3" drawbar="805020000"/>
	  <note measure="5" beat="4" duration="1" note="A3" drawbar="805020000"/>
	  <note measure="7" beat="1" duration="0.75" note="G3" drawbar="805020000"/>
	  <note measure="7" beat="3" duration="0.75" note="E3" drawbar="805020000"/>
	  <note measure="8" beat="1" duration="0.75" note="A3" drawbar="805020000"/>
	  <note measure="8" beat="4" duration="0.75" note="E3" drawbar="805020000"/>
	  <note measure="9" beat="4" duration="0.75" note="G3" drawbar="805020000"/>
	  <note measure="9" beat="4" duration="0.75" note="G3" drawbar="805020000"/>
	  <note measure="9" beat="4" duration="0.75" note="G3" drawbar="805020000"/>
	  <note measure="9" beat="4" duration="0.75" note="G3" drawbar="805020000"/>
	  <note measure="11" beat="1" duration="0.75" note="E3" drawbar="805020000"/>
	  <note measure="11" beat="2" duration="0.75" note="E3" drawbar="805020000"/>
	  <note measure="11" beat="3" duration="0.75" note="E3" drawbar="805020000"/>
	  <note measure="11" beat="4" duration="0.75" note="E3" drawbar="805020000"/>
	  <note measure="13" beat="1" duration="0.75" note="G3" drawbar="805020000"/>
	  <note measure="13" beat="2" duration="0.75" note="C3" drawbar="805020000"/>
	  <note measure="13" beat="3" duration="0.75" note="E3" drawbar="805020000"/>
	  <note measure="13" beat="4" duration="1" note="A3" drawbar="805020000"/>
      <note measure="14" beat="1" duration="0.75" note="G3" drawbar="805020000"/>
	  <note measure="14" beat="2" duration="0.75" note="C3" drawbar="805020000"/>
	  <note measure="14" beat="3" duration="0.75" note="E3" drawbar="805020000"/>
	  <note measure="14" beat="4" duration="1" note="A3" drawbar="805020000"/>
  </instrument>

  <instrument instrument="AdditiveInstrument"> 
  <note measure="3" beat="1" duration="0.75" harmonics="1 0 0 0 0" envelope=".05 0.85 .99 .01" note="G3"/>
       <note measure="3" beat="2" duration="0.75" harmonics="1 0 0 0 0" envelope=".05 0.85 .99 .01" note="C3"/>
       <note measure="3" beat="3" duration="0.75" harmonics="1 0 0 0 0" envelope=".05 0.85 .99 .01" note="E3"/>
       <note measure="3" beat="4" duration="1" harmonics="1 0 0 0 0" envelope=".05 0.85 .99 .01" note="A3"/>
       <note measure="4" beat="1" duration="0.75" harmonics="1 0 0 0 0" envelope=".05 0.85 .99 .01" note="G3"/>
       <note measure="4" beat="2" duration="0.75" harmonics="1 0 0 0 0" envelope=".05 0.85 .99 .01" note="C3"/>
       <note measure="4" beat="3" duration="0.75" harmonics="1 0 0 0 0" envelope=".05 0.85 .99 .01" note="E3"/>
       <note measure="4" beat="4" duration="1" harmonics="1 0 0 0 0" envelope=".05 0.85 .99 .01" note="A3"/>
	   <note measure="6" beat="1" duration="0.75" harmonics="1 0 0 0 0" envelope=".05 0.85 .99 .01" note="G3"/>
       <note measure="6" beat="2" duration="0.75" harmonics="1 0 0 0 0" envelope=".05 0.85 .99 .01" note="C3"/>
       <note measure="6" beat="3" duration="0.75" harmonics="1 0 0 0 0" envelope=".05 0.85 .99 .01" note="E3"/>
       <note measure="6" beat="4" duration="1" harmonics="1 0 0 0 0" envelope=".05 0.85 .99 .01" note="A3"/>
       <note measure="7" beat="2" duration="0.75" harmonics="1 0 0 0 0" envelope=".05 0.85 .99 .01" note="C3"/>
       <note measure="7" beat="4" duration="1" harmonics="1 0 0 0 0" envelope=".05 0.85 .99 .01" note="A3"/>
	   <note measure="8" beat="2" duration="0.75" harmonics="1 0 0 0 0" envelope=".05 0.85 .99 .01" note="C3"/>
       <note measure="8" beat="4" duration="1" harmonics="1 0 0 0 0" envelope=".05 0.85 .99 .01" note="A3"/>
	   <note measure="10" beat="1" duration="0.75" harmonics="1 0 0 0 0" envelope=".05 0.85 .99 .01" note="C3"/>
       <note measure="10" beat="2" duration="0.75" harmonics="1 0 0 0 0" envelope=".05 0.85 .99 .01" note="C3"/>
       <note measure="10" beat="3" duration="0.75" harmonics="1 0 0 0 0" envelope=".05 0.85 .99 .01" note="C3"/>
       <note measure="10" beat="4" duration="0.75" harmonics="1 0 0 0 0" envelope=".05 0.85 .99 .01" note="C3"/>
	   <note measure="12" beat="1" duration="0.75" harmonics="1 0 0 0 0" envelope=".05 0.85 .99 .01" note="A3"/>
	   <note measure="12" beat="2" duration="0.75" harmonics="1 0 0 0 0" envelope=".05 0.85 .99 .01" note="A3"/>
	   <note measure="12" beat="3" duration="0.75" harmonics="1 0 0 0 0" envelope=".05 0.85 .99 .01" note="A3"/>
	   <note measure="12" beat="4" duration="0.75" harmonics="1 0 0 0 0" envelope=".05 0.85 .99 .01" note="A3"/>
	   <note measure="13" beat="1" duration="0.75" harmonics="1 0 0 0 0" envelope=".05 0.85 .99 .01" note="G3"/>
       <note measure="13" beat="2" duration="0.75" harmonics="1 0 0 0 0" envelope=".05 0.85 .99 .01" note="C3"/>
       <note measure="13" beat="3" duration="0.75" harmonics="1 0 0 0 0" envelope=".05 0.85 .99 .01" note="E3"/>
       <note measure="13" beat="4" duration="1" harmonics="1 0 0 0 0" envelope=".05 0.85 .99 .01" note="A3"/>
	   <note measure="14" beat="1" duration="0.75" harmonics="1 0 0 0 0" envelope=".05 0.85 .99 .01" note="G3"/>
       <note measure="14" beat="2" duration="0.75" harmonics="1 0 0 0 0" envelope=".05 0.85 .99 .01" note="C3"/>
       <note measure="14" beat="3" duration="0.75" harmonics="1 0 0 0 0" envelope=".05 0.85 .99 .01" note="E3"/>
       <note measure="14" beat="4" duration="1" harmonics="1 0 0 0 0" envelope=".05 0.85 .99 .01" note="A3"/>
  </instrument>
</score>
```


[1]: https://gitlab.msu.edu/pagadamf/multi-component-music-synthesizer/-/blob/main/Becca.md?ref_type=heads "Becca Winkler"
[2]: https://gitlab.msu.edu/pagadamf/multi-component-music-synthesizer/-/tree/main/Step5?ref_type=heads%2F "Ajuisiwon Azantilow"
[3]: https://gitlab.msu.edu/pagadamf/multi-component-music-synthesizer/-/blob/main/Calisa.md?ref_type=heads "Calisa Xu Stevens"
[4]: https://gitlab.msu.edu/pagadamf/multi-component-music-synthesizer/-/tree/main/Step5?ref_type=heads%2F "Fredrick Pagadam"