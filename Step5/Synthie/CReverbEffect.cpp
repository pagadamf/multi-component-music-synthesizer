#include "stdafx.h"
#include "CReverbEffect.h"

CReverbEffect::CReverbEffect()
{
}

CReverbEffect::~CReverbEffect()
{
}

void CReverbEffect::Process(double* input, double* output)
{
	mQueueL[mWrloc] = input[0];
	mQueueR[mWrloc] = input[1];

	int delaySamples = mDelay * GetSampleRate();
	int sampleIdx = (mRdloc + QSize - delaySamples) % QSize;

	output[0] = input[0] + mWet * mQueueL[sampleIdx];
	output[1] = input[0] + mWet * mQueueR[sampleIdx];

	mRdloc = (mRdloc + 1) % QSize;
	mWrloc = (mWrloc + 1) % QSize;
}

void CReverbEffect::Start()
{
	mRdloc = mWrloc = 0;
}

bool CReverbEffect::Generate()
{
	return true;
}
