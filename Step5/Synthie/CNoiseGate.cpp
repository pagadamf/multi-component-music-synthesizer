#include "stdafx.h"
#include "CNoiseGate.h"

CNoiseGate::CNoiseGate()
{
}

CNoiseGate::~CNoiseGate()
{
}

void CNoiseGate::Process(double* input, double* output)
{
	if (abs(input[0]) < mThreshold)
	{
		output[0] = 0;
		output[1] = 0;
	}
	else
	{
		output[0] = input[0];
		output[1] = input[1];
	}

}

void CNoiseGate::Start()
{
}

bool CNoiseGate::Generate()
{
	return true;
}
