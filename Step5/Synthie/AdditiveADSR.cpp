#include "stdafx.h"
#include "AdditiveADSR.h"



AdditiveADSR::AdditiveADSR()
{
	m_duration = 0.1;
	m_attack = 0.05;
	m_release = 0.05;
	m_decay = 1;
	m_sustain = 1;
	
}


AdditiveADSR::~AdditiveADSR()
{
}


void AdditiveADSR::Start()
{
	m_source->SetSampleRate(GetSampleRate());
	m_source->Start();
	m_time = 0;
}

bool AdditiveADSR::Generate()
{
	if (m_crossfade && m_time > m_crossfadeStartTime && m_time < m_fadeduration + m_fadeStartTime) {
		m_source->GenerateCrossFade(m_fadeduration, m_time);
		
	}
	else {
		m_source->Generate();
	}
	


	//Attack
	if (m_time <= m_attack)
	{
		m_frame[0] = m_source->Frame(0) * (m_time / m_attack);
		m_frame[1] = m_source->Frame(1) * (m_time / m_attack);
	}

	//Decay 
	else if (m_time > m_attack && m_time <= m_attack + m_decay)
	{
		m_frame[0] = m_source->Frame(0) * (m_duration - m_time) / m_decay;
		m_frame[1] = m_source->Frame(1) * (m_duration - m_time) / m_decay;
	}

	//Release 
	else if (m_time > m_duration - m_release)
	{
		m_frame[0] = m_source->Frame(0) * ((m_duration - m_time) / m_release);
		m_frame[1] = m_source->Frame(1) * ((m_duration - m_time) / m_release);
	}

	//Sustain
	else
	{
		m_frame[0] = m_source->Frame(0);
		m_frame[1] = m_source->Frame(1);
	}

	
	m_time += GetSamplePeriod();


	return m_time < m_duration;
}
