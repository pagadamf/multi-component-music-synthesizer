#pragma once
#include "Instrument.h"
#include "Notes.h"
#include "AR.h"
#include <vector>
#include "OrganEnvelope.h"

using namespace std;

class COrgan :
	public CInstrument
{
public:
	COrgan();
	COrgan(double bpm);
	virtual ~COrgan() {};

	const double NUM_SECS_IN_MINUTE = 60.0;
	virtual void Start();
	virtual bool Generate();

	
	double GetFrame(int i) {
		return m_frame[i];
	}
	virtual void SetNote(CNote* note);
	void COrgan::SetOrganDrawbars(int drawbarNum);

	void SetFrequency(double frequency) { m_frequency = frequency; }
	void SetDuration(double duration) { m_envelope.SetDuration(duration); m_duration = duration; }
private:
	double m_duration;
	double m_time;
	CAR m_ar;
	double m_frequency;
	double m_vibratoFrequency;
	double m_vibratoMagnitude;
	double m_attack;
	double m_release;
	double m_startFrequency;
	double m_endFrequency;
	double m_radius;
	double m_amp;
	double m_phase;
	double m_vibratoPhase;
	double m_speakerPhase;
	double m_speakerFrequency;
	vector<double> m_tonewheels;
	COrganEnvelope m_envelope;
	vector<double> m_drawbars;
};

