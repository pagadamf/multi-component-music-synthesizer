#pragma once
#include "CAudioEffect.h"
class CChorusEffect :
    public CAudioEffect
{
public:
    CChorusEffect();

    ~CChorusEffect();

    void Process(double* input, double* output) override;

    void Start() override;

    bool Generate() override;
};

