
#include "stdafx.h"
#include "AdditiveGenerator.h"
#include <iostream>
double RangeBound(double d)
{
	if (d < -32768.0000)
		return -32768.0000;
	else if (d > 32767.0000)
		return 32767.0000;

	return d;
}

AdditiveGenerator::AdditiveGenerator()
{

}


AdditiveGenerator::~AdditiveGenerator()
{
}

void AdditiveGenerator::Start()
{
	m_phase = 0;
	m_vibrato = 0;
}

bool AdditiveGenerator::Generate()
{
	double waveform = GenerateSineWave();

	m_frame[1] = m_frame[0] = waveform;


	//	Update phase and vibrato
	m_vibrato += 2 * PI * m_vibRate * GetSamplePeriod();
	m_phase += 2 * PI * (m_freq + m_vibFreq * sin(m_vibrato)) * GetSamplePeriod();

	return true;
}

double AdditiveGenerator::GenerateSineWave() {
	double waveform = 0;
	const int numHarmonics = 5.0;

	for (int harmonic = 0; harmonic < numHarmonics; harmonic++)
	{
		waveform += m_harmonics[harmonic] * sin(m_phase * (harmonic + 1) + sin(m_vibrato));
		
		waveform = RangeBound(waveform);
	
	}
	return waveform;
}

double AdditiveGenerator::GenerateCrossFade(double duration, double time) {

	if (m_nextwave != nullptr) {
		double interpolated = GenerateSineWave() * ((duration - (time - m_fadestarttime)) / duration) + m_nextwave->Generate() * (time - m_fadestarttime) / m_duration;

		m_frame[1] = m_frame[0] = interpolated;
		//	Update phase and vibrato
		m_vibrato += 2 * PI * m_vibRate * GetSamplePeriod();
		m_phase += 2 * PI * (m_freq + m_vibFreq * sin(m_vibrato)) * GetSamplePeriod();

	}
	else {
		Generate();

	}
	return 0.0;
}

