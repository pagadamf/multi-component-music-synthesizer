#pragma once
#include "Instrument.h"
#include <vector>

/**
* Class serves as the base effect class from which all other effects are derived
**/
class CAudioEffect :
    public CInstrument
{
public:
    CAudioEffect();

	virtual ~CAudioEffect();

	virtual void Process(double* frameIn, double* frameOut) = 0;

	void SetDelay(double delay) { mDelay = delay; }

	void SetWet(double wet) { mWet = wet; }

	void SetDry(double dry) { mDry = dry; }

	void SetThreshold(double threshold) { mThreshold = threshold; }

	virtual void SetNote(CNote* note) override;
protected:
	// dry input audio
	double mDry;

	// processed effect audio
	double mWet;

	// the audio delay
	double	mDelay;
	
	// the treshold for effects that find this useful
	double  mThreshold;

	// the write location for the queue
	int mWrloc;

	// the read location for the queue
	int mRdloc;

	std::vector<double> mQueueL;
	std::vector<double> mQueueR;

	// maximum queue size
	const int QSize = 200000;
};
