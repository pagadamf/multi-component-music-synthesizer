#include "stdafx.h"
#include "PianoEnvelope.h"


CPianoEnvelope::CPianoEnvelope(void)
{
    m_release = 0.01;
    m_duration = 0.1;
    m_attack = 0.05;
    m_decay = 0.05;
    m_source = NULL;
}

CPianoEnvelope::~CPianoEnvelope()
{
}
void CPianoEnvelope::Start()
{
    m_time = 0;
}

/*
* This stimulates what happens when a key is released.
* A dampening sound
*/
bool CPianoEnvelope::Generate()
{
    //double output;
    //double m_time = 0.0;
    //double m_duration = m_wave.size() / 44100.;

    //for (unsigned int i = 0; i < m_wave.size(); i++, m_time += 1 / 44100.)
    //{
        //if (m_time < m_attack)
        //{
            // attacking
            //output = m_wave[i] * (1 - exp(-5 * m_time / m_attack)) * m_volume;
       // }
       // else if (m_time > (m_duration - m_release))
       // {
            // releasing
            //double releaseTime = m_duration - m_time;
           // output = m_wave[i] * exp(-5 * releaseTime / m_release) * m_volume;
        //}
       // else
       // {
       //     output = m_wave[i] * m_volume;
      //  }

       // m_wave[i] = short(output);
   // }CPianoEnvelope::~CPianoEnvelope()

    double gain = 1;

    if (m_time < m_attack) {
        // Attacking phase
        gain = 2 * (m_time / m_attack);
    }
    else if (m_time >= m_attack && m_time < (m_attack + m_decay)) {
        // Decay phase
        double decayTime = m_time - m_attack;
        gain = 2 - (decayTime / m_decay);
    }
    else if (m_time > (m_duration - m_release) && m_time < m_duration) {
        // Releasing phase
        double releaseTime = m_time - (m_duration - m_release);
        gain = 1 - (releaseTime / (-m_release));
    }
    else if (m_time >= m_duration) {
        // Past the duration, set frames to 0 and return false
        m_frame[0] = m_frame[1] = 0;
        return false;
    }

    if (m_source != NULL) {
        m_frame[0] = m_source->Frame(0) * gain;
        m_frame[1] = m_source->Frame(1) * gain;
    }

    m_time += GetSamplePeriod();
    return true;
}