#include "stdafx.h"
#include "OrganEnvelope.h"


COrganEnvelope::COrganEnvelope(void)
{
	m_attack = 0.05;
	m_decay = 0.05;
	m_release = 0.01;
	m_duration = 0.1;
	m_source = NULL;
}


void COrganEnvelope::Start()
{
	m_time = 0;
}

bool COrganEnvelope::Generate()
{
	double gain = 1;
	// If we are not yet at the attack point set the gain
	if (m_time < m_attack){   gain = 2 * (m_time / m_attack);  }
	//if were are past the attack point and the time is less than our attack and delay
	else if (m_time > m_attack && m_time < (m_attack + m_decay)){   gain = 2 - ((m_time - m_attack) / m_decay);  }
	//if we are past the duration and release
	else if (m_time > (m_duration - m_release) && m_time < m_duration) { gain = 1 - (m_time - (m_duration - m_release)) / (-m_release); }
	// if it's past the duration, set frames to 0 and return false (should not be generating)
	else if (m_time >= m_duration) {  m_frame[0] = m_frame[1] = 0; return false;  }



	//if we have a source, set the frames to the source frames multiplieds by the gain
	if (m_source != NULL)
	{
		m_frame[0] = m_source->Frame(0) * gain;
		m_frame[1] = m_source->Frame(1) * gain;
	}



	m_time += GetSamplePeriod();
	return true;
}