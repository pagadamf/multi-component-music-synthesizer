#include "stdafx.h"
#include "Piano.h"
#include "Notes.h"
#include "audio/DirSoundSource.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>


CPiano::CPiano()
{
	m_duration = 0.1;
	m_pedal = false;
	m_samples = nullptr;
	m_numsamples = 0;
}


CPiano::~CPiano()
{
	// releasing of memory 
	if (m_samples != nullptr)
	{
		delete[] m_samples;
		m_samples = nullptr;
	}
}
// Sets the beats
CPiano::CPiano(double bpm)
{
	m_bpm = bpm;
	m_duration = 0.1;

}


void CPiano::Start() {
	// Initialize audio generation
	StartPianoPlayer();
}

bool CPiano::Generate()
{
	// Generate audio data and return true while notes are playing

	return GeneratePianoPlayer();
}

//This is the way for notes to be played. More than one note can be played
// at a time
void CPiano::SetNote(CNote* note)
{

	// Get a list of all attribute nodes and the
	// length of that list
	CComPtr<IXMLDOMNamedNodeMap> attributes;
	note->Node()->get_attributes(&attributes);
	long len;
	attributes->get_length(&len);

	// Loop over the list of attributes
	for (int i = 0; i < len; i++)
	{
		// Get attribute i
		CComPtr<IXMLDOMNode> attrib;
		attributes->get_item(i, &attrib);

		// Get the name of the attribute
		CComBSTR name;
		attrib->get_nodeName(&name);

		// Get the value of the attribute.  A CComVariant is a variable
		// that can have any type. It loads the attribute value as a
		// string (UNICODE), but we can then change it to an integer 
		// (VT_I4) or double (VT_R8) using the ChangeType function 
		// and then read its integer or double value from a member variable.
		CComVariant value;
		attrib->get_nodeValue(&value);
		if (name == "note")
		{
			value.ChangeType(VT_BSTR);
			m_frequency = NoteToFrequency(value.bstrVal);
			SetFreq(NoteToFrequency(value.bstrVal));


		}
		else if (name == "duration")
		{
			value.ChangeType(VT_R8);
			m_duration = value.dblVal;
			SetDuration(value.dblVal * (60 / m_bpm));
		}

	}

	//Set the duration to include the time it takes for a note to "dampen" out after releasing the key
	m_duration += m_release;

	if (!m_pedal) {
		ChangeDuration();
	}

}

/* Change the duration of the note to be what is supplied in the score*/

void CPiano::StartPianoPlayer()
{
	m_position = 0; // Initialize position 
	m_time = 0;


	// call the envelope here to set source, samplerate, and starting
	m_envelope.SetSource(this);
	m_envelope.SetSampleRate(GetSampleRate());
	m_envelope.Start();

}

bool CPiano::GeneratePianoPlayer()
{
	if (m_position < m_numsamples)
	{
		if (m_pedal)
		{
			// If the pedal is down, mix the samples from m_samples and m_pedalWave
			m_frame[0] = m_samples[m_position] / 32768.0 + m_pedalWave[m_position] / 32768.0;
			m_frame[1] = m_frame[0];
		}
		else
		{
			// If the pedal is up, use only the samples from m_samples
			m_frame[0] = m_samples[m_position] / 32768.0;
			m_frame[1] = m_frame[0];
		}

		m_position++;
	}
	else
	{
		m_frame[1] = m_frame[0] = 0;
		return false;
	}
	return true;
}

bool CPiano::PlayPedalUp() {
	// Handle pedal release
	int duration = 0.5;
	m_pedalWave.clear();

	for (int i = 0; i < duration; i++)
	{
		
		short sample = static_cast<short>(32767 * sin(2 * PI * i * 440.0 / 44100));
		m_pedalWave.push_back(sample);
	}

	return true;
}
bool CPiano::PlayPedalDown() {
	int duration = 0.5;
	m_pedalWave.clear();

	for (int i = 0; i < duration; i++)
	{
		// Create a simple waveform (you can customize this)
		short sample = static_cast<short>(32767 * sin(2 * 3.14159265359 * i * 440.0 / 44100));
		m_pedalWave.push_back(sample);
	}

	return true;
}

void CPiano::ChangeDuration()
{
	PlayPedalUp();
}
