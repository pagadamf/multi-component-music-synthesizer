#include "stdafx.h"
#include "AdditiveInstrument.h"
#include <string>
#include <vector>

#include <iostream>
#include <sstream>
#include "Notes.h"



using namespace std;

void AdditiveInstrument::Start() {
	m_envelopeprocessor.SetSource(&m_generator);
	m_envelopeprocessor.SetSampleRate(GetSampleRate());
	m_envelopeprocessor.Start();

}

bool AdditiveInstrument::Generate()
{
	bool valid = m_envelopeprocessor.Generate();

	m_frame[0] = m_envelopeprocessor.Frame(0);
	m_frame[1] = m_envelopeprocessor.Frame(1);

	m_time += GetSamplePeriod();
	return valid;
}

void AdditiveInstrument::SetNote(CNote* note) {
	// Get a list of all attribute nodes and the
// length of that list
	CComPtr<IXMLDOMNamedNodeMap> attributes;
	note->Node()->get_attributes(&attributes);
	long len;
	attributes->get_length(&len);

	// Loop over the list of attributes
	for (int i = 0; i < len; i++)
	{
		// Get attribute i
		CComPtr<IXMLDOMNode> attrib;
		attributes->get_item(i, &attrib);

		// Get the name of the attribute
		CComBSTR name;
		attrib->get_nodeName(&name);

		CComVariant value;
		attrib->get_nodeValue(&value);

		//Effect finder section
		if (name == "duration")
		{
			value.ChangeType(VT_R8);
			m_envelopeprocessor.SetDuration(value.dblVal);
		}
		else if (name == "note")
		{
			m_generator.SetFreq(NoteToFrequency(value.bstrVal));
		
		}
		else if (name == "fadeduration")
		{
			value.ChangeType(VT_R8);
			m_envelopeprocessor.SetFadeStartTime(value.dblVal);

		}
		else if (name == "harmonics") {

			VibratoAndHarmonics(value, name);

		}
		else if (name == "envelope"){
				ADSRProcessor(value);
		}

		else if (name == "vibrato")
		{
			VibratoAndHarmonics(value, name);

		}



	}
}

void AdditiveInstrument::VibratoAndHarmonics(CComVariant value, CComBSTR name) {
	wstring str(value.bstrVal);
	string vibratoharmonics(str.begin(), str.end());
	istringstream iss(vibratoharmonics);
	vector<string> values;
	string token;
	while (iss >> token) {
		values.push_back(token);
	}

	if (name == "harmonics") {
		for (int i = 0; i < values.size(); i++) {
			m_amplitude = stod(values[i]);
			m_generator.SetHarmonics(i, m_amplitude);
		}
	}
	else if ( name == "vibrato" && values.size() >= 2) {
		m_rate = stod(values[0]);
		m_freq = stod(values[1]);
		m_generator.SetVibratoRate(m_rate);
		m_generator.SetVibratoFreq(m_freq);

	}

	
}


void AdditiveInstrument::ADSRProcessor(CComVariant value) 
	{
	wstring str(value.bstrVal);
	string adsr(str.begin(), str.end());

	istringstream iss(adsr);
	string token;
	vector<string> adsrValues;

	while (iss >> token) {
		adsrValues.push_back(token);
	}

	if (adsrValues.size() >= 4) {
		m_envelopeprocessor.SetAttack(stod(adsrValues[0]));
		m_envelopeprocessor.SetRelease(stod(adsrValues[1]));
		m_envelopeprocessor.SetDecay(stod(adsrValues[2]));
		m_envelopeprocessor.SetSustain(stod(adsrValues[3]));
	}

}

void AdditiveInstrument::SetNextNote(CNote* note) {

	AdditiveInstrument* instrument = NULL;

	instrument = new AdditiveInstrument();
	instrument->SetSampleRate(GetSampleRate());
	instrument->SetNote(note);
	instrument->Start();
	
	m_generator.SetNextWave(instrument->GetSource());
}
