#include "stdafx.h"
#include "PianoFactory.h"
#include "Piano.h"
using namespace std;

//constructor
PianoFactory::PianoFactory()
{
	// setting beginning values
	m_volume = 1.0;
	m_duration = 1.0;
	m_pedal = false;
	m_advDynamic = false;
}
//deconstructor
PianoFactory::~PianoFactory()
{
}

// Makes the piano instrument 
CPiano* PianoFactory::CreateInstrument(double bpm)
{
	CPiano* instrument = new CPiano(bpm);

	instrument->SetPedal(m_pedal);
	instrument->SetVolume(m_volume);
	instrument->SetDuration(m_duration);

	if (m_pedal) {
		if (m_pedalUp) {
			instrument->PlayPedalUp();
			m_pedalUp = false;
		}
		else {
			instrument->PlayPedalDown();
			m_pedalDown = false;
		}

	}

	return instrument;
}
//This is the way for notes to be played. More than one note can be played
// at a time
void PianoFactory::SetNote(CNote* note)
{

	// Get a list of all attribute nodes and the
	// length of that list
	CComPtr<IXMLDOMNamedNodeMap> attributes;
	note->Node()->get_attributes(&attributes);
	long len;
	attributes->get_length(&len);

	// Loop over the list of attributes
	for (int i = 0; i < len; i++)
	{
		// Get attribute i
		CComPtr<IXMLDOMNode> attrib;
		attributes->get_item(i, &attrib);

		// Get the name of the attribute
		CComBSTR name;
		attrib->get_nodeName(&name);

		// Get the value of the attribute.  A CComVariant is a variable
		// that can have any type. It loads the attribute value as a
		// string (UNICODE), but we can then change it to an integer 
		// (VT_I4) or double (VT_R8) using the ChangeType function 
		// and then read its integer or double value from a member variable.
		CComVariant value;
		attrib->get_nodeValue(&value);

		if (name == "note")
		{
			value.ChangeType(VT_BSTR);


		}
		else if (name == "duration")
		{
			value.ChangeType(VT_R8);
			m_duration = value.dblVal;
		}
		else if (name == "pedal")
		{
			value.ChangeType(VT_BSTR);
			char pedal[100];
			wcstombs(pedal, value.bstrVal, 100);
			if (std::string(pedal) == "released") {
				m_pedalUp = true;
				m_pedal = false;
			}
			else if (std::string(pedal) == "pressed") {
				m_pedalDown = true;
				m_pedal = true;
			}
		else if (name == "dynamic") {

		}
		// Piano sound when played loud 
		//else if (name == "advdynamic")
		//{
			//value.ChangeType(VT_BSTR);
		//}	
		}
	}
}
