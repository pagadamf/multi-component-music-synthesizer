#pragma once
#include "AudioNode.h"
#include "Instrument.h"
#include "audio/DirSoundSource.h"
#include "AR.h"
#include <vector>
/// Envelope of the piano
/// Recorded piano samples always assume the key is just pressed and held.
/// You need to simulate what happens when the key is released.  
/// Note that this is not just a cutoff of the sound, but rather a dampening.
/// 
class CPianoEnvelope : public CAudioNode
{
public:
	CPianoEnvelope();
	virtual ~CPianoEnvelope();

	virtual void Start();

	virtual bool Generate();

	void SetSource(CAudioNode* source) { m_source = source; }
	void SetAttack(double attack) { m_attack = attack; }
	void SetRelease(double release) { m_release = release; }
	void SetDecay(double decay) { m_decay = decay; }

	void SetDuration(double duration) { m_duration = duration; }


private:
	CAudioNode* m_source;
	/// this is how long the sample will be 
	double m_duration;
	/// Need to keep notice of time
	double m_time;
	double m_attack;
	double m_decay;
	double m_release;
	double m_volume;

};

