#pragma once
#include "AudioNode.h"

class COrganEnvelope :
	public CAudioNode
{
public:
	COrganEnvelope();
	virtual ~COrganEnvelope() {};

	virtual void Start();
	virtual bool Generate();

	void SetSource(CAudioNode* source) { m_source = source; }
	void SetAttack(double attack) { m_attack = attack; }
	void SetRelease(double release) { m_release = release; }
	void SetDecay(double decay) { m_decay = decay; }
	void SetDuration(double duration) { m_duration = duration; }

private:
	double m_time;
	double m_attack;
	double m_release;
	double m_decay;
	double m_duration;
	CAudioNode* m_source;
};