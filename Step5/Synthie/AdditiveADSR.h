#pragma once
#include "AudioNode.h"
#include "AR.h" 
#include "AdditiveGenerator.h"
class AdditiveADSR : public CAR

{
public:
	AdditiveADSR();
	virtual ~AdditiveADSR();

public:
	void SetSource(AdditiveGenerator* const& source) { m_source = source; }
	void SetDuration(double duration) { m_duration = duration; }
	void SetAttack(double attack) { m_attack = attack; }
	void SetRelease(double release) { m_release = release; }
	void SetDecay(double decay) { m_decay = decay; }
	void SetSustain(double sustain) { m_sustain = sustain; }
	virtual void Start();
	virtual bool Generate();
	void SetCrossFade() { m_crossfade = true; }

	void SetFadeStartTime(double fadestart) {
		m_fadeStartTime = m_duration - fadestart;
		m_fadeduration = fadestart;
		m_source->SetFadeStartTime(m_fadeStartTime);
		; }
	void SetFadeOut(double fadeout) { m_fadeOut = fadeout; }

private:
	double m_duration;
	double m_time;
	double m_attack;
	double m_release;
	double m_decay;
	double m_sustain;
	bool m_crossfade = false;

  AdditiveGenerator* m_source;

	double m_fadeStartTime = 0.0;
	double m_fadeOut;
	double m_crossfadeStartTime = 0.0;
	double m_crossfadeEndTime = 0.0;
	double m_fadeduration = 0.0;
};