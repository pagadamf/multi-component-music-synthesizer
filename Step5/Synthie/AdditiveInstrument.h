#pragma once
#include "Instrument.h"

#include "AdditiveGenerator.h"
#include "SineWave.h"
#include "AdditiveADSR.h"

class AdditiveInstrument :
    public CInstrument
{
private:
    double m_rate = 0.0;
    double m_freq = 0.0;
    double m_time = 0.0;
    double m_duration = 0.0;
    double m_amplitude = 0.0;

    AdditiveGenerator m_generator;
    AdditiveADSR m_envelopeprocessor;

public:
    virtual void Start();
    virtual bool Generate();
    virtual void SetNote(CNote* note);
    void VibratoAndHarmonics(CComVariant value, CComBSTR name);
    void ADSRProcessor(CComVariant value);
    void SetNextNote(CNote* note);
    void SetDuration(double duration) { m_duration = duration; }

    AdditiveGenerator GetSource() { return m_generator; }
 
};

