#include "stdafx.h"
#include "CFlangeEffect.h"

CFlangeEffect::CFlangeEffect()
{
}

CFlangeEffect::~CFlangeEffect()
{
}

void CFlangeEffect::Process(double* input, double* output)
{
    int delay = GetSampleRate() * mDelay;
    int delayIdx = int(mRdloc - mDelay + QSize) % QSize;

    double delayVal = delay / 2.0 * (1 + sin(2 * PI * 0.25 * mRdloc / GetSampleRate()));
    delayIdx = (mRdloc - delayVal + QSize);

    double interpolationAmount = delayVal - int(delay);
    output[0] = (1 - interpolationAmount) * input[0] + interpolationAmount * input[delayIdx];
}

void CFlangeEffect::Start()
{
    mRdloc = mWrloc = 0;
}

bool CFlangeEffect::Generate()
{
    return true;
}
