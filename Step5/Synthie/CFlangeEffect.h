#pragma once
#include "CAudioEffect.h"
class CFlangeEffect :
    public CAudioEffect
{
public:
    CFlangeEffect();

    ~CFlangeEffect();

    void Process(double* input, double* output) override;

    void Start() override;

    bool Generate() override;
};

