#pragma once
#include "AudioNode.h"
#include "Note.h"
class CInstrument :
	public CAudioNode
{
public:
	CInstrument();
	CInstrument(double);
	virtual ~CInstrument();
	virtual void SetNote(CNote *note) = 0;
	virtual void SetNextNote(CNote* note);
	void SetSend(int idx, double val) { mSends[idx] = val; }
	double Send(int idx) { return mSends[idx]; }
private:
	double mSends[5];
};

