#pragma once
#include "CAudioEffect.h"
class CNoiseGate :
    public CAudioEffect
{
public:
    CNoiseGate();

    ~CNoiseGate();

    void Process(double* input, double* output) override;

    void Start() override;

    bool Generate() override;
};

