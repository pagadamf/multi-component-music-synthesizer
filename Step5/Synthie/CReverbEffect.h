#pragma once
#include "CAudioEffect.h"
class CReverbEffect :
    public CAudioEffect
{
public:
    CReverbEffect();

    ~CReverbEffect();

    void Process(double* input, double* output) override;

    void Start() override;

    bool Generate() override;
};

