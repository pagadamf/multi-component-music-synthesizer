//Similar to base

#pragma once
#include "AudioNode.h"

class AdditiveGenerator :
	public CAudioNode
{
public:
	AdditiveGenerator();
	virtual ~AdditiveGenerator();

public:
	virtual void Start();
	virtual bool Generate();

	void SetFreq(double f) { m_freq = f; }
	void SetHarmonics(int i, double a) { m_harmonics[i] = a; }

	//vibrato rate/freq
	void SetVibratoRate(double r) { m_vibRate = r; }
	void SetVibratoFreq(double f) { m_vibFreq = f; }

	void SetDuration(double d) { m_duration = d; }
    double GenerateSineWave();
	double GenerateCrossFade(double duration, double time);
	
	void SetNextWave(AdditiveGenerator wave) { m_nextwave = &wave; }
	void SetFadeStartTime(double time) { m_fadestarttime = time; }

private:
	double m_fadestarttime = 0.0;
	double m_duration = 0;
	double m_freq;
	double m_phase = 0;
	double m_harmonics[5] = { 0 };

	double m_vibrato = 0;
	double m_vibRate = 0;
	double m_vibFreq = 0;

	AdditiveGenerator* m_nextwave = nullptr;


};

