#pragma once
#include "Instrument.h"
#include "AR.h"
#include "Notes.h"
#include "PianoEnvelope.h"

class CPiano:
	public CInstrument
{
public:
	CPiano();
	CPiano(double bpm);
	virtual ~CPiano();


	const double NUM_SECS_IN_MINUTE = 60.0;
	virtual void Start();
	virtual bool Generate();


	virtual void SetNote(CNote* note);

	void GetFrame(double frame[2]) { frame[0] = m_frame[0]; frame[1] = m_frame[1]; }

	bool PlayPedalUp();

	bool PlayPedalDown();

	// for this connect it to the envelope to set duration 
	void SetDuration(double d) { m_envelope.SetDuration(d); m_duration = d; }
	void ChangeDuration();

	void SetFreq(double f) { m_frequency = f; }

	void SetPedal(bool pedal) { m_pedal = pedal; }

	void SetVolume(double vol) { m_volume = vol; }

	//std::wstring GetName() const { return L"Piano"; }
private:
	CAR m_ar;

	short* m_samples;
	double m_frequency;
	bool m_pedal;
	double m_time;
	double m_duration;
	double m_attack;
	double m_release;
	double m_volume;
	int m_position;
	int m_numsamples;
	CPianoEnvelope m_envelope;
	std::wstring m_type;
	std::vector<short> m_pedalWave;

	void StartPianoPlayer();
	bool GeneratePianoPlayer();
};


