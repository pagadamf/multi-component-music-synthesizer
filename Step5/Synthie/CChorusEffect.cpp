#include "stdafx.h"
#include "CChorusEffect.h"

CChorusEffect::CChorusEffect()
{
}

CChorusEffect::~CChorusEffect()
{
}

void CChorusEffect::Process(double* input, double* output)
{
	double delayVariance = 2 * mDelay * sin(2 * PI * 0.25);
	double newDelay = mDelay + delayVariance;

	mWrloc = (mWrloc + 1) % QSize;
	mQueueL[mWrloc] = input[0];
	mQueueR[mWrloc] = input[1];

	int delayLength = int((newDelay * m_sampleRate + 0.5)) * 2;
	int rdloc = (mWrloc + QSize - delayLength) % QSize;

	output[0] = mDry * input[0] + mWet * mQueueL[rdloc];
	output[1] = mDry * input[1] + mWet * mQueueR[rdloc];
}

void CChorusEffect::Start()
{
	mRdloc = mWrloc = 0;
}

bool CChorusEffect::Generate()
{
	return true;
}
