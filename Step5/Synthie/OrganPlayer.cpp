#include "stdafx.h"
#include "OrganPlayer.h"
#include <cmath>

using namespace std;

COrganPlayer::COrganPlayer()
{
	m_time = 0;

	m_drawbars.resize(9, 0.0);
	m_drawbars[0] = 8.0;
	//m_drawbars = { 16,5.5,8,0, };
	
	m_toneWheels = { 1,3,2,4,6,8,10,12,16 };
}

COrganPlayer::~COrganPlayer()
{
}

void COrganPlayer::Start()
{
	m_phase = 0;
	m_vibrato_phase = 0;
}

bool COrganPlayer::Generate()
{
	auto rate = GetSampleRate();
	auto period = GetSamplePeriod();
	double vibMag = (m_vibratoMagnitude / 100.0) * m_freq;
	double sample = 0;

	// generate a sample from the 9 tonewheels
	for (int i = 0; i < 9; i++) {
		sample += m_drawbars[i] * sin(m_phase * m_toneWheels[i]);
	}


	m_frame[1] = m_frame[0] = sample * m_amp;
	
	double speakerDiff = (1 + (m_radius * (cos(m_speakerPhase) / 340.3)));
	double vibratoDiff = vibMag * sin(m_vibrato_phase);
	m_phase += 2 * PI * ((m_freq + vibratoDiff) * speakerDiff) * period;
	m_vibrato_phase += 2 * PI * m_vibratoFreq * period;

	// If we're not at the attack time yet, update phase
	if (m_time < (m_speakerAttack)) {
		m_speakerPhase += 2 * PI * (m_speakerStart + ((m_leslieFreq - m_speakerStart) * (m_time / m_speakerAttack))) * period;
	}
	// if were somewhere inbetwen the duration and before the release and attack
	else if (m_time > (m_duration - (m_speakerRelease))) {
		auto release = m_duration - (m_speakerRelease);
		m_speakerPhase += 2 * PI * (m_speakerEnd + ((m_leslieFreq - m_speakerEnd) * (1 - ((m_time - release) / (m_duration - release))))) * period;
		m_speakerStart = m_speakerEnd;
	}

	else {
		m_speakerPhase += 2 * PI * m_leslieFreq * period;
	}

	m_time++;

	return true;
}