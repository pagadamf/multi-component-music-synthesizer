#pragma once
#include "Piano.h"

class PianoFactory
{
public:
	//constructor and deconstructor 
	PianoFactory();
	virtual ~PianoFactory();

	// creating of the piano and notes 
	CPiano* CreateInstrument(double bpm);
	void SetNote(CNote* note);
private:
	double m_volume;
	double m_duration;
	bool m_pedal;
	bool m_advDynamic;
	bool m_pedalDown;
	bool m_pedalUp;
};

