#include "stdafx.h"
#include "Organ.h"
#include "Notes.h"

COrgan::COrgan()
{
	m_duration = 0.1;
}

// Constructor to set the beats per minute
COrgan::COrgan(double bpm)
{
	m_bpm = bpm;
	m_duration = 0.1;
	m_amp = 0.5;
	m_attack = 0;
	m_speakerFrequency = 0;
	m_radius = 0;
	m_amp = 0.5;
	m_vibratoFrequency = 5;
	m_tonewheels = { 1,3,2,4,6,8,10,12,16 };
}



void COrgan::Start()
{

	m_time = 0;
	m_phase = 0;
	m_vibratoPhase = 0;
	m_speakerPhase = 0;
	m_vibratoMagnitude = (0.01) * m_frequency;
	m_envelope.SetSource(this);
	m_envelope.SetSampleRate(GetSampleRate());
	m_envelope.Start();



}


bool COrgan::Generate()
{

	// Get the sample from the toneweheels
	double sample = 0;
	for (int i = 0; i < 9; i++) {
		sample += m_drawbars[i] * sin(m_phase * m_tonewheels[i]);
	}

	if (m_time > 0.5)
	{
		int i = 0;
	}


	// Set the frame to the tonewheel samples multiplied by the amplitude
	m_frame[1] = m_frame[0] = sample * m_amp;
	auto difference = (1 + (m_radius * (cos(m_speakerPhase) / 340.3)));
	m_phase += 2 * PI * ((m_frequency + (m_vibratoMagnitude * sin(m_vibratoPhase)) * difference) * GetSamplePeriod());
	m_vibratoPhase += 2 * PI * m_vibratoFrequency * GetSamplePeriod();





	// If we're not at the attack time yet, use the starting frequencies
	if (m_time < (m_attack)) {
		m_speakerPhase += 2 * PI * (m_startFrequency + ((m_speakerFrequency - m_startFrequency) * (m_time / m_attack))) * GetSamplePeriod();
	}




	// if were somewhere inbetwen the duration and before the release and duration time calculate the phase using end frequencies
	else if (m_time > (m_duration - (m_release))) {
		auto releaseTime = m_duration - (m_release);
		auto temp = (1 - ((m_time - releaseTime) / (m_duration - releaseTime)));
		m_speakerPhase += 2 * PI * (m_endFrequency + ((m_speakerFrequency - m_endFrequency) * temp)) * GetSamplePeriod();
		m_startFrequency = m_endFrequency;
	}



	// Otherwise, calculate the phase by the typical speaker frequency
	else {
		m_speakerPhase += 2 * PI * m_speakerFrequency * GetSamplePeriod();
	}



	
	bool valid = m_envelope.Generate();
	m_frame[0] = m_envelope.Frame(0);
	m_frame[1] = m_envelope.Frame(1);
	m_time += GetSamplePeriod();

	return valid;
}

void COrgan::SetOrganDrawbars(int drawbar)
{
	m_drawbars = { 0,0,0,0,0,0,0,0,0 };
	int digit = 0;
	double sample = 0.0;
	for (int i = 0; i < 9; i++) { 

		// Calculates the indivdual digit of the integer
		digit = drawbar % (int)pow((double)10, (9 - i));
		digit = digit / (int)pow((double)10, (8 - i));
		if (digit != 0)
		{
			m_drawbars[i] = pow(10.0, (((8 - (double)digit) * -3) / 20));
		}
	}
}


void COrgan::SetNote(CNote* note)
{
	// Get a list of all attribute nodes and the
	// length of that list
	CComPtr<IXMLDOMNamedNodeMap> attributes;
	note->Node()->get_attributes(&attributes);
	long len;
	attributes->get_length(&len);

	// Loop over the list of attributes
	for (int i = 0; i < len; i++)
	{
		// Get attribute i
		CComPtr<IXMLDOMNode> attrib;
		attributes->get_item(i, &attrib);

		// Get the name of the attribute
		CComBSTR name;
		attrib->get_nodeName(&name);

		// Get the value of the attribute.  A CComVariant is a variable
		// that can have any type. It loads the attribute value as a
		// string (UNICODE), but we can then change it to an integer 
		// (VT_I4) or double (VT_R8) using the ChangeType function 
		// and then read its integer or double value from a member variable.
		CComVariant value;
		attrib->get_nodeValue(&value);

		if (name == "duration")
		{
			value.ChangeType(VT_R8);
			SetDuration(value.dblVal * (60/ m_bpm));
		}
		else if (name == "note")
		{
			SetFrequency(NoteToFrequency(value.bstrVal));
			m_frequency = NoteToFrequency(value.bstrVal);
		}
		else if (name == "drawbar")
		{
			value.ChangeType(VT_I4);
			SetOrganDrawbars(value.intVal);
		}
	}

}